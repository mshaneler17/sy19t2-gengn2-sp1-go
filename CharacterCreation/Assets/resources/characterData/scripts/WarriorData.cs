﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Types;

public class WarriorData : CharacterData
{
    public WarriorClassType classType;
    public WarriorWpnType wpnType;
}
