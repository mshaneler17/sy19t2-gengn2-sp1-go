﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Types;

public class RogueData : CharacterData
{
    public RogueStrategyType strategyType;
    public RogueWpnType wpnType;
}
