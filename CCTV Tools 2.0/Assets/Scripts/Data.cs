﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Data : ScriptableObject
{
    public GameObject cCamera;
    public int xPixels;
    public int yPixels;
    public string cName;
}
