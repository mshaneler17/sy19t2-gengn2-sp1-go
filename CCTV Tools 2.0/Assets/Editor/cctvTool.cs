﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;
using System.Security.Cryptography.X509Certificates;

public class cctvTool : EditorWindow
{
    public GameObject cCamera;
    public RenderTexture renTex;
    Texture2D headerSectionTexture;
    Texture2D mainSectionTexture;

    Color headerSectionColor = new Color(113f / 255f, 132f / 255f, 144f / 255f, 1f);
    Color mainSectionColor = new Color(50f / 255f, 50f / 255f, 50f / 255f, 1f);

    Rect headerSection;
    Rect mainSection;

    [MenuItem("Window/CCTV Tool")]
    static void OpenWindow()
    {
        cctvTool window = (cctvTool)GetWindow(typeof(cctvTool));
        window.minSize = new Vector2(300, 200);
        window.Show();
    }
    void OnEnable()
    {
        InitTextures();
    }
    void InitTextures()
    {
        headerSectionTexture = new Texture2D(1, 1);
        headerSectionTexture.SetPixel(0, 0, headerSectionColor);
        headerSectionTexture.Apply();

        mainSectionTexture = new Texture2D(1, 1);
        mainSectionTexture.SetPixel(0, 0, mainSectionColor);
        mainSectionTexture.Apply();
    }
    void OnGUI()
    {
        DrawLayouts();
        DrawHeader();
        DrawMain();
    }
    void DrawLayouts()
    {
        //top
        headerSection.x = 0;
        headerSection.y = 0;
        headerSection.width = Screen.width;
        headerSection.height = 50;
        //main
        mainSection.x = 0;
        mainSection.y = 50;
        mainSection.width = Screen.width;
        mainSection.height = Screen.height - 50;
    }
    void DrawHeader()
    {
        GUILayout.BeginArea(headerSection);
        GUILayout.Label("CCTV Tool");

        GUILayout.EndArea();
    }
    void DrawMain()
    {
        GUILayout.BeginArea(mainSection);
        GUILayout.Label("Menu");

        EditorGUILayout.BeginHorizontal();
        GUILayout.Label("Camera");
        cCamera = (GameObject)EditorGUILayout.ObjectField(cCamera, typeof(GameObject), false);
        EditorGUILayout.EndHorizontal();

        if(cCamera == null) //assuming it assigns cameras
        {
            EditorGUILayout.HelpBox("Requires a camera to create a monitor.", MessageType.Warning);
        }
        else if (GUILayout.Button("Finish and Save", GUILayout.Height(30)))
        {
            Connecting();
            window.Close();
        }
    }
    void Connecting()
    {
        renTex = new RenderTexture(1920, 1080, 16, RenderTextureFormat.ARGB32);
        renTex.Create();
    }
}
